import threading
from bot.handlers import *


def main():
    updater_thread = threading.Thread(target=update_animal_saturation)
    updater_thread.start()

    bot.infinity_polling()


if __name__ == "__main__":
    main()