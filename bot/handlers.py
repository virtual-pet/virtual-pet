import telebot
from telebot import types
from Token.secret import token
from .scrape import *
from db.manager import MongoDbInteraface, MongoDbUserInteraface
from weather.weather_api import Weather_manager
from weather.api_token import weather_token
from PIL import Image
import matplotlib.pyplot as plt
from .aux_functions import *
import io
import time


bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def begin(message):

    MongoDbUserInteraface().addUser(message.from_user.id, message.from_user.first_name)
    animal_id = MongoDbUserInteraface().getAnimal(message.from_user.id,)

    if animal_id != None:
        photo_caption = f"Hi, {message.from_user.first_name}. Welcome back :)"
        obj = MongoDbInteraface().getAnimalById(animal_id)
        bot.send_photo(message.chat.id, obj["image"], caption=photo_caption)
        animal_name = MongoDbUserInteraface().getAnimalName(message.from_user.id, )
        if animal_name == None:
            bot.send_message(message.chat.id, "Lets name the pet!", reply_markup=give_a_name_button())

    else:
        photo_caption = f"Hi, {message.from_user.first_name}. Choose your pet."
        for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("sphynx", "room"):
            bot.send_photo(message.chat.id, obj["image"], caption=photo_caption)

        kb_inline_pets = types.InlineKeyboardMarkup()

        button_cat = types.InlineKeyboardButton("Cat", callback_data="cat")
        button_dog = types.InlineKeyboardButton("Dog", callback_data="dog")
        button_hamster = types.InlineKeyboardButton("Hamster", callback_data="hamster")
        kb_inline_pets.add(button_cat, button_dog, button_hamster)
        bot.send_message(message.chat.id, "Please select an animal:", reply_markup=kb_inline_pets)


@bot.message_handler(func=lambda message: message.content_type == 'text' and not message.text.startswith(
    '/') and message.text.strip().isdigit())
def handle_fact_input(message):
    if not MongoDbUserInteraface().checkIfExist(message.from_user.id):
        MongoDbUserInteraface().addUser(message.from_user.id, message.from_user.first_name)

    animal_id = MongoDbUserInteraface().getAnimal(message.from_user.id)
   
    if animal_id != None:
        animal = MongoDbInteraface().getAnimalById(animal_id)     
        animal_type = animal["animal_type"]
        
        if animal_type == "cat":
            user_input = message.text.strip()
            if user_input.isdigit():
                fact_number = int(user_input)
                explanation = scrape_cat_explanation(fact_number)

                bot.reply_to(message, f"{explanation}")
        elif animal_type == "dog":
            user_input = message.text.strip()
            if user_input.isdigit():
                fact_number = int(user_input)
                explanation = scrape_dog_explanation(fact_number)

                bot.reply_to(message, f"{explanation}")
        else:
            user_input = message.text.strip()
            if user_input.isdigit():
                fact_number = int(user_input)
                explanation = scrape_hamster_explanation(fact_number)

                bot.reply_to(message, f"{explanation}")
    else:
        bot.send_message(message.chat.id, "Please select an animal")




@bot.callback_query_handler(func=lambda callback: callback.data == 'cat')
def check_callback_data(callback):
    button_siamese = types.InlineKeyboardButton("Siamese", callback_data="siamese")
    button_sphynx = types.InlineKeyboardButton("Sphynx", callback_data="sphynx")
    button_oriental = types.InlineKeyboardButton("Oriental long-haired", callback_data="oriental")
    kb_inline_cats_breeds = types.InlineKeyboardMarkup()
    kb_inline_cats_breeds.add(button_oriental, button_siamese, button_sphynx)

    for obj in MongoDbInteraface().getAnimalsByTypeAndRoom("cat", "room"):
        bot.send_photo(callback.message.chat.id, obj["image"])
    bot.send_message(callback.message.chat.id, "Choose a breed", reply_markup=kb_inline_cats_breeds)


@bot.callback_query_handler(func=lambda callback: callback.data == 'dog')
def check_callback_data(callback):
        button_beagle = types.InlineKeyboardButton("Beagle", callback_data="beagle")
        button_azawakh = types.InlineKeyboardButton("Azawakh", callback_data="azawakh")
        button_afghan = types.InlineKeyboardButton("Afghan hound", callback_data="afghan")
        kb_inline_dogs_breeds = types.InlineKeyboardMarkup()
        kb_inline_dogs_breeds.add(button_afghan, button_azawakh, button_beagle)

        for obj in MongoDbInteraface().getAnimalsByTypeAndRoom("dog","room"):
            bot.send_photo(callback.message.chat.id, obj["image"])
        bot.send_message(callback.message.chat.id, "Choose a breed", reply_markup=kb_inline_dogs_breeds)


@bot.callback_query_handler(func=lambda callback: callback.data == 'hamster')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByTypeAndRoom("hamster","room"):
        bot.send_photo(callback.message.chat.id, obj["image"], caption="hamster is chosen")

        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'siamese')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("siamese","room"):
        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_photo(callback.message.chat.id, obj["image"], caption="siamese cat is chosen")
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'sphynx')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("sphynx","room" ):
        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_photo(callback.message.chat.id, obj["image"], caption="sphynx cat is chosen")
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'oriental')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("oriental","room"):
        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_photo(callback.message.chat.id, obj["image"], caption="oriental cat is chosen")
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'beagle')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("beagle","room" ):
        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_photo(callback.message.chat.id, obj["image"], caption="beagle dog is chosen")
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'azawakh')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("azawakh","room"):
        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_photo(callback.message.chat.id, obj["image"], caption="azawakh dog is chosen")
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'afghan')
def check_callback_data(callback):
    for obj in MongoDbInteraface().getAnimalsByBreedAndRoom("afghan","room"):
        if not MongoDbUserInteraface().checkIfExist(callback.from_user.id):
            MongoDbUserInteraface().addUser(callback.from_user.id, callback.from_user.first_name)

        MongoDbUserInteraface().assignAnimal(callback.from_user.id, obj["_id"])
        bot.send_photo(callback.message.chat.id, obj["image"], caption="afghan dog is chosen")
        bot.send_message(callback.message.chat.id, "Now lets name the pet!", reply_markup=give_a_name_button())


@bot.callback_query_handler(func=lambda callback: callback.data == 'give_a_name')
def check_callback_data(callback):
    sent = bot.send_message(callback.message.chat.id, "Please provide a name:")
    bot.register_next_step_handler(callback.message, response)


@bot.callback_query_handler(func=lambda callback: callback.data == 'food')
def check_callback_data(callback):
    MongoDbUserInteraface().setFoodLevel(callback.message.chat.id, 20)
    bot.send_message(callback.message.chat.id, "food level: +20%")


@bot.callback_query_handler(func=lambda callback: callback.data == 'water')
def check_callback_data(callback):
    MongoDbUserInteraface().setFoodLevel(callback.message.chat.id, 20)
    bot.send_message(callback.message.chat.id, "water level: +20%")


@bot.callback_query_handler(func=lambda callback: callback.data == 'level')
def check_callback_data(callback):
    food_level = MongoDbUserInteraface().getFoodLevel(callback.message.chat.id)
    water_level = MongoDbUserInteraface().getFoodLevel(callback.message.chat.id)
    mess = "food level: " + str(food_level) + "% water level: " + str(water_level) + "%"
    bot.send_message(callback.message.chat.id, mess)


@bot.message_handler(commands=['menu'])
def open_menu(message):
    actions = types.ReplyKeyboardMarkup(resize_keyboard=True)

    walk = types.KeyboardButton("/walk")
    feed = types.KeyboardButton("/feed")
    wash = types.KeyboardButton("/wash")
    weather = types.KeyboardButton("/show_weather")
    show_fact = types.KeyboardButton("/show_facts")
    get_statistics = types.KeyboardButton("/get_statistics")

    actions.add(walk, feed, wash, weather, show_fact, get_statistics)
    bot.send_message(message.chat.id, "Menu:", reply_markup=actions)


@bot.message_handler(commands=['show_facts'])
def show__facts(message):
    if not MongoDbUserInteraface().checkIfExist(message.from_user.id):
            MongoDbUserInteraface().addUser(message.from_user.id, message.from_user.first_name)

    animal_id = MongoDbUserInteraface().getAnimal(message.from_user.id)

    if animal_id is not None:
        animal = MongoDbInteraface().getAnimalById(animal_id)
        type_of = animal["animal_type"]
        if type_of == "cat":
            cat_list = scrape_cat_love_facts()
            numbered_facts = "\n\n".join([f"{i+1}. {fact}" for i, fact in enumerate(cat_list)])
            bot.send_message(message.chat.id, "Here are some signs that your cat loves you:\n\n" + numbered_facts)
        elif type_of == "dog":
            dog_list = scrape_dog_facts()
            numbered_facts = "\n\n".join([f"{i+1}. {fact}" for i, fact in enumerate(dog_list)])
            bot.send_message(message.chat.id, "Which dog breed is the best?\n\n" + numbered_facts)
        elif type_of == "hamster":
            ham_list = scrape_hamster_facts()
            numbered_facts = "\n\n".join([f"{i + 1}. {fact}" for i, fact in enumerate(ham_list)])
            bot.send_message(message.chat.id, "Everything you need to know about hamsters:\n\n" + numbered_facts)


@bot.message_handler(commands=['walk'])
def go_for_a_walk(message):
    text = "That was totally pawsome! My tail's still waggin' from all the cool stuff we sniffed out and saw on our adventure. Feeling pumped and chill, I'm ready to crash in my cozy spot and dream about our next walkies. \n\nYo, thanks for being my ride-or-die on these adventures, human. You're the best!"

    if not MongoDbUserInteraface().checkIfExist(message.from_user.id):
        MongoDbUserInteraface().addUser(message.from_user.id, message.from_user.first_name)

    animal_id = MongoDbUserInteraface().getAnimal(message.from_user.id)
    if animal_id is not None:
        animal = MongoDbInteraface().getAnimalById(animal_id)
        breed = animal["breed"]
        for obj in MongoDbInteraface().getAnimalsByBreedAndRoom(breed=breed, room_type="outdoor"):
            bot.send_photo(message.chat.id, obj["image"], caption=text)
    else:
        bot.send_message(message.chat.id, "You do not have an animal yet :(")


@bot.message_handler(commands=['feed'])
def feed_pet(message):
    animal_id = MongoDbUserInteraface().getAnimal(message.from_user.id)
    food_level = MongoDbUserInteraface().getFoodLevel(message.chat.id)
    water_level = MongoDbUserInteraface().getFoodLevel(message.chat.id)
    if animal_id != None:
        if food_level <= 30 or water_level <=30:
            mess = "Feed me immediately!"
        else:
            mess = "What would you like to do?"
        button_food = types.InlineKeyboardButton("Give some food", callback_data="food")
        button_water = types.InlineKeyboardButton("Give some water", callback_data="water")
        button_current_level = types.InlineKeyboardButton("Show a current level", callback_data="level")
        kb_feed = types.InlineKeyboardMarkup()
        kb_feed.add(button_food, button_water,button_current_level)
        bot.send_message(message.chat.id, mess, reply_markup=kb_feed)
    else:
        bot.send_message(message.chat.id, "You do not have a pet yet. Go to start.")


@bot.message_handler(commands=['wash'])
def wash_pet(message):
    text = "Hooray! I love how fresh and fluffy I feel after that bath! My fur's like a soft cloud, and I'm ready to show off my squeaky-clean self. \n\nHooray for bath time, it's like a spa day just for me!"

    if not MongoDbUserInteraface().checkIfExist(message.from_user.id):
        MongoDbUserInteraface().addUser(message.from_user.id, message.from_user.first_name)

    animal_id = MongoDbUserInteraface().getAnimal(message.from_user.id)
    if animal_id is not None:
        animal = MongoDbInteraface().getAnimalById(animal_id)
        breed = animal["breed"]
        for obj in MongoDbInteraface().getAnimalsByBreedAndRoom(breed=breed, room_type="bathroom"):
            bot.send_photo(message.chat.id, obj["image"], caption=text)
    else:
        bot.send_message(message.chat.id, "You do not have an animal yet :(")


@bot.message_handler(commands=['weather'])
def show_recent_weather(message):
    city = bot.send_message(message.chat.id, "Please enter your city: ")
    bot.register_next_step_handler(city, get_city)


@bot.message_handler(commands=['get_statistics'])
def get_statistics(message):
    breed_counts = {}
    for obj in MongoDbUserInteraface().getAllUsers():
        id = MongoDbUserInteraface().getAnimal(obj["_id"])

        if (id != None):
            animal = MongoDbInteraface().getAnimalById(id)
            breed = animal["breed"]
            if breed in breed_counts:
                breed_counts[breed] += 1
            else:
                breed_counts[breed] = 1
    sorted_breeds = sorted(breed_counts.keys(), key=lambda x: breed_counts[x], reverse=True)
    sorted_counts = [breed_counts[breed] for breed in sorted_breeds]

    buf = bar_chart(sorted_breeds, sorted_counts)
    bot.send_photo(message.chat.id, photo=Image.open(buf), caption="Look!")
