import matplotlib.pyplot as plt
from db.manager import MongoDbInteraface, MongoDbUserInteraface
import time
import io
def give_a_name_button():
    name_button = types.InlineKeyboardButton("Give me a name", callback_data="give_a_name")
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(name_button)
    return keyboard


def process_name(message):
    if message.content_type == 'text':
        name = message.text
        bot.send_message(message.chat.id, f"Thank you for the name: {name}")
    else:
        bot.send_message(message.chat.id, "Please provide a valid name. Text messages only.")


def get_city(message):
    city_name = message.text

    caption = Weather_manager().get_weather_data(weather_token, city_name)
    graph = Weather_manager().get_forecast_graph(weather_token, city_name)

    if caption and graph:
        bot.send_photo(message.chat.id, photo=Image.open(graph), caption=caption)
    else:
        bot.send_message(message.chat.id, "Data not found")

def response(message):
    name = message.text
    MongoDbUserInteraface().setAnimalName(message.chat.id, name)
    bot.send_message(message.chat.id, "The name is set.")


def update_animal_saturation():
    mongo_interface = MongoDbUserInteraface()
    while True:
        mongo_interface.decFoodLevelAll(-10)
        mongo_interface.decWaterLevelAll(-20)
        time.sleep(86400)

def bar_chart(height, weight):
    plt.switch_backend('agg')

    plt.figure(figsize=(10, 6))
    plt.bar(height, weight, color='orange')
    plt.xlabel('Breeds')
    plt.ylabel('How many users chose')
    plt.title('Count of Different Breeds')

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close()
    buf.seek(0)

    return buf