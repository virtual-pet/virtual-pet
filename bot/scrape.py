from bs4 import BeautifulSoup
import requests 


def scrape_cat_love_facts():
    timeout = 35
    url = "https://www.vitalpetclub.co.uk/signs-that-your-cat-loves-you/"

    response = requests.get(url, timeout=timeout)
    soup = BeautifulSoup(response.content, "html.parser")

    fact_elements = soup.find_all("div", class_="col-lg-8")[0].find_all("strong")[2:]
    fact_titles = [element.get_text(strip=True) for element in fact_elements]

    return fact_titles


def scrape_cat_explanation(fact_number):
    timeout = 35
    url = "https://www.vitalpetclub.co.uk/signs-that-your-cat-loves-you/"
    response = requests.get(url, timeout=timeout)
    soup = BeautifulSoup(response.content, "html.parser")

    explanation_elements = soup.find("div", class_="col-lg-8").find_all("p")

    explanation_elements = explanation_elements[2:]

    if fact_number >= 1 and fact_number <= len(explanation_elements):
        return explanation_elements[fact_number - 1].get_text(strip=True)


def scrape_dog_facts():
    timeout = 35
    url = "https://www.vitalpetclub.co.uk/which-dog-breed-is-best-for-me/"

    response = requests.get(url, timeout=timeout)
    soup = BeautifulSoup(response.content, "html.parser")

    fact_elements = soup.find_all("div", class_="col-lg-8")[0].find_all("strong")[1:]
    fact_titles = [element.get_text(strip=True) for element in fact_elements]

    return fact_titles


def scrape_dog_explanation(fact_number):
    timeout = 35
    url = "https://www.vitalpetclub.co.uk/which-dog-breed-is-best-for-me/"
    response = requests.get(url, timeout=timeout)
    soup = BeautifulSoup(response.content, "html.parser")

    explanation_elements = soup.find("div", class_="col-lg-8").find_all("p")[1:]
    
    if fact_number >= 1 and fact_number <= len(explanation_elements):
        return explanation_elements[fact_number - 1].get_text(strip=True)


def scrape_hamster_facts():
    timeout = 35
    url = "https://www.vitalpetclub.co.uk/everything-you-need-to-know-about-hamsters/"

    response = requests.get(url, timeout=timeout)
    soup = BeautifulSoup(response.content, "html.parser")

    fact_elements = soup.find_all("div", class_="col-lg-8")[0].find_all("strong")[1:]
    fact_titles = [element.get_text(strip=True) for element in fact_elements]

    return fact_titles


def scrape_hamster_explanation(fact_number):
    timeout = 35
    url = "https://www.vitalpetclub.co.uk/everything-you-need-to-know-about-hamsters/"
    response = requests.get(url, timeout=timeout)
    soup = BeautifulSoup(response.content, "html.parser")

    explanation_elements = soup.find("div", class_="col-lg-8").find_all("p")

    explanation_elements = [element for index, element in enumerate(explanation_elements) if index not in [3, 8, 12, 14, 16]]
    explanation_elements = explanation_elements[2:]

    if 1 <= fact_number <= len(explanation_elements):
        return explanation_elements[fact_number - 1].get_text(strip=True)