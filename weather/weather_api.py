import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline
import numpy as np
import requests
from datetime import datetime
import io



class Weather_manager:
    base_url = "http://api.openweathermap.org/data/2.5/"
    
    def weather_request(self, api, city_name):
        r = requests.get(self.base_url + "weather?&units=metric&appid=" + api + "&q=" + city_name)
        data = r.json()
        
        if int(data["cod"]) != 200:
            return None
        return data

    def forecast_request(self, api, city_name):
        r = requests.get(self.base_url + "forecast?&units=metric&appid=" + api + "&q=" + city_name)
        data = r.json()
        
        if int(data["cod"]) != 200:
            return None
        return data


    def get_weather_data(self, api, city_name):
        data = self.weather_request(api, city_name)
   
        if data:
            return f"Your weather {data['weather'][0]['main']}, temperature is {data['main']['temp']}°C, but feels like {data['main']['feels_like']}°C "
    

    def get_forecast_graph(self, api, city_name):
        data = self.forecast_request(api, city_name)
   
        if data:    
            numeric_time = []
            temp = []
            
            for i in data["list"]:
                numeric_time.append((datetime.strptime(i["dt_txt"], "%Y-%m-%d %H:%M:%S")).timestamp())
                temp.append(i["main"]["temp"])
            
            plt.switch_backend('agg')

            SplineXY = make_interp_spline(numeric_time, temp)    
            X_ = np.linspace(min(numeric_time), max(numeric_time), 500)
            
            inter_temp = SplineXY(X_)
            inter_time = [datetime.fromtimestamp(timestamp) for timestamp in X_] 
            
            plt.plot(inter_time, inter_temp, '-g')
            plt.xlabel('Time')
            plt.ylabel('Temperature')
            
            plt.title(f'Temperature in {city_name}')

            buf = io.BytesIO()
            plt.savefig(buf, format='png')
            plt.close()
            buf.seek(0)
            
            return buf

