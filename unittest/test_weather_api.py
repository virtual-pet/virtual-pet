import io
import unittest
from weather.api_token import weather_token
from weather.weather_api import Weather_manager


class TestWeatherManager(unittest.TestCase):

    def setUp(self):
        self.weather_manager = Weather_manager()

    def test_weather_request(self):
        result = self.weather_manager.weather_request(weather_token, "London")
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)

    def test_forecast_request(self):
        result = self.weather_manager.forecast_request(weather_token, "London")
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)

    def test_get_weather_data(self):
        result = self.weather_manager.get_weather_data(weather_token, "London")
        self.assertIsNotNone(result)
        self.assertIsInstance(result, str)

    def test_get_forecast_graph(self):
        result = self.weather_manager.get_forecast_graph(weather_token, "London")
        self.assertIsNotNone(result)
        self.assertIsInstance(result, io.BytesIO)


if __name__ == '__main__':
    unittest.main()
