import unittest
from unittest.mock import patch, MagicMock
from bson.objectid import ObjectId
from db.manager import MongoDbInteraface, MongoDbConnection

class TestMongoDbInteraface(unittest.TestCase):

    @patch('db.manager.MongoDbConnection')
    def setUp(self, MockMongoDbConnection):
        self.mock_conn = MagicMock()
        MockMongoDbConnection().__enter__.return_value = self.mock_conn
        self.mongo_interface = MongoDbInteraface(mongodb_context_man=MockMongoDbConnection())

        self.test_animal = {
            'name': 'TestName',
            'animal_type': 'TestType',
            'breed': 'TestBreed',
            'room_type': 'TestRoom',
            'greetings': 'TestHello',
            'sounds': 'TestSound'
        }
        self.test_animal_id = self.insert_test_animal(self.test_animal)

    def insert_test_animal(self, animal_data):
        self.mock_conn['virtual_pet']['animal'].insert_one.return_value.inserted_id = ObjectId()
        animal_id = self.mock_conn['virtual_pet']['animal'].insert_one(animal_data).inserted_id
        return animal_id

    def test_getAllAnimals(self):
        self.mock_conn['virtual_pet']['animal'].find.return_value = [self.test_animal]
        animal_list = self.mongo_interface.getAllAnimals()
        self.assertIsNotNone(animal_list)
        self.assertIsInstance(animal_list, list)

    def test_getAnimalsByType(self):
        self.mock_conn['virtual_pet']['animal'].find.return_value = [self.test_animal]
        animal_list = self.mongo_interface.getAnimalsByType("TestType")
        self.assertIsNotNone(animal_list)
        self.assertIsInstance(animal_list, list)

    def test_getAnimalsByBreed(self):
        self.mock_conn['virtual_pet']['animal'].find.return_value = [self.test_animal]
        animal_list = self.mongo_interface.getAnimalsByBreed("TestBreed")
        self.assertIsNotNone(animal_list)
        self.assertIsInstance(animal_list, list)

    def test_getAnimalsByRoomType(self):
        self.mock_conn['virtual_pet']['animal'].find.return_value = [self.test_animal]
        animal_list = self.mongo_interface.getAnimalsByRoomType("TestRoom")
        self.assertIsNotNone(animal_list)
        self.assertIsInstance(animal_list, list)

    def test_getAnimalById(self):
        self.mock_conn['virtual_pet']['animal'].find_one.return_value = self.test_animal
        animal = self.mongo_interface.getAnimalById(self.test_animal_id)
        self.assertIsNotNone(animal)
        self.assertIsInstance(animal, dict)

    def test_getAnimalsByBreedAndRoom(self):
        self.mock_conn['virtual_pet']['animal'].find.return_value = [self.test_animal]
        animal_list = self.mongo_interface.getAnimalsByBreedAndRoom("TestBreed", "TestRoom")
        self.assertIsNotNone(animal_list)
        self.assertIsInstance(animal_list, list)

    def test_getAnimalsByTypeAndRoom(self):
        self.mock_conn['virtual_pet']['animal'].find.return_value = [self.test_animal]
        animal_list = self.mongo_interface.getAnimalsByTypeAndRoom("TestType", "TestRoom")
        self.assertIsNotNone(animal_list)
        self.assertIsInstance(animal_list, list)

    def test_getGreetingsById(self):
        self.mock_conn['virtual_pet']['animal'].find_one.return_value = self.test_animal
        greetings = self.mongo_interface.getGreetingsById(self.test_animal_id)
        self.assertIsNotNone(greetings)
        self.assertIsInstance(greetings, str)

    def test_getSoundsById(self):
        self.mock_conn['virtual_pet']['animal'].find_one.return_value = self.test_animal
        sounds = self.mongo_interface.getSoundsById(self.test_animal_id)
        self.assertIsNotNone(sounds)
        self.assertIsInstance(sounds, str)

if __name__ == '__main__':
    unittest.main()
