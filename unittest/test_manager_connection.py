import unittest
from unittest.mock import patch, MagicMock
from db.manager import MongoDbConnection

class TestMongoDbConnection(unittest.TestCase):

    # def test_connnection(self):
    #     self.assertIsNotNone(MongoDbConnection())

    @patch('db.manager.MongoClient')  
    def test_mongo_db_connection(self, mock_mongo_client):

        mock_client = MagicMock()
        mock_mongo_client.return_value = mock_client

        with MongoDbConnection('localhost', 27017) as connection:

            self.assertEqual(connection, mock_client)

            mock_mongo_client.assert_called_with('localhost', 27017)
        
        mock_client.close.assert_called_once()

if __name__ == '__main__':
    unittest.main()
