from pymongo import MongoClient
from bson.objectid import ObjectId
from .model import User, Animal


class MongoDbConnection:

        def __init__(self, host = 'localhost', port = 27017):
            self.host = host
            self.port = port
            self.connection = None
        
        def __enter__(self):
            self.connection = MongoClient(self.host, self.port)
            return self.connection
        
        def __exit__(self, exc_type, exc_val, exc_tb):
            self.connection.close()

class MongoDbInteraface:
    def __init__(self, db_name = None, mongodb_context_man = None):
        self.db_name = db_name if db_name else "virtual_pet"
        self.mongodb_context_man = mongodb_context_man if mongodb_context_man else MongoDbConnection()    
    
    def getAllAnimals(self):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]
            
            return list(collection.find({}))

    def getAnimalsByType(self, animal_type):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]
            
            return list(collection.find({"animal_type": animal_type}))

    def getAnimalsByBreed(self, breed):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]
            
            return list(collection.find({"breed": breed}))


    def getAnimalsByRoomType(self, room_type):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]

            return list(collection.find({"room_type": room_type}))

    def getAnimalById(self, id): 
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]
            
            return collection.find_one({"_id": ObjectId(id)})
    def getAnimalsByBreedAndRoom(self, breed, room_type):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]

            return list(collection.find({"breed": breed, "room_type": room_type}))

    def getAnimalsByTypeAndRoom(self, animal_type, room_type):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["animal"]

            return list(collection.find({"animal_type": animal_type, "room_type": room_type}))
    def getGreetingsById(self, id):
        return self.getAnimalById(id)["greetings"]

    def getSoundsById(self, id):
        return self.getAnimalById(id)["sounds"]

class MongoDbUserInteraface:
    def __init__(self, db_name = None, mongodb_context_man = None):
        self.db_name = db_name if db_name else "virtual_pet"
        self.mongodb_context_man = mongodb_context_man if mongodb_context_man else MongoDbConnection()  

    def addUser(self, id, name):
        if(self.checkIfExist(id)):
            return None
        
        User.objects.create(_id = id, user_name=name)

    def setAnimalName(self, id, anim_name):
        User.objects(_id = id).update(animal_name = anim_name)
    
    def getWaterLevel(self, id):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]

            return collection.find_one({"_id": id})["a_water_level"]

    def getFoodLevel(self, id):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]

            return collection.find_one({"_id": id})["a_food_level"]


    def setWaterLevel(self, id, value):
        water_level = self.getWaterLevel(id)
        
        if (water_level == 100 and value >= 0) or (water_level==0 and value <= 0):
            return 0
        
        if water_level+value < 0:
            User.objects(_id = id).update(a_water_level = 0)
            return 1


        if water_level + value >= 100:
            User.objects(_id = id).update(a_water_level = 100)
        elif water_level+value <= 0:
            User.objects(_id = id).update(a_water_level = 0)        
        else:
            User.objects(_id = id).update(a_water_level = water_level+value)
        
        return 1
    



    def setFoodLevel(self, id, value):
        food_level = self.getFoodLevel(id)
        
        if (food_level == 100 and value >= 0) or (food_level == 0 and value <= 0):
            return 0
        
        if(food_level + value >= 100):
            User.objects(_id = id).update(a_food_level = 100)
        elif(food_level + value <= 0):
            User.objects(_id = id).update(a_food_level = 0)
        else:
            User.objects(_id = id).update(a_food_level = food_level+value)
        
        return 1

    
    def decFoodLevelAll(self, neg_value):
        if neg_value > 0:
            return None
        
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]

            collection.update_many({"a_food_level": {"$gte": abs(neg_value)}}, 
                       {"$inc": {"a_food_level": neg_value}})
            
            collection.update_many({"a_food_level": {"$lt": abs(neg_value)}}, 
                       {"$set": {"a_food_level": 0}})
        

    def decWaterLevelAll(self, neg_value):
        if neg_value > 0:
            return None
        
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]

            collection.update_many({"a_water_level": {"$gte": abs(neg_value)}}, 
                       {"$inc": {"a_water_level": neg_value}})
    
            collection.update_many({"a_water_level": {"$lt": abs(neg_value)}}, 
                       {"$set": {"a_water_level": 0}})


    def assignAnimal(self, user_id, animal_id):
        animal = Animal.objects.get(id = ObjectId(animal_id))
        User.objects(_id = user_id).update(animal = animal)

    def checkIfExist(self, id):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]
            
            return len(list(collection.find({"_id": id}))) != 0

    def getAnimalName(self, id):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]
            
            return collection.find_one({"_id": id}).get("animal_name")
    
    def getAnimal(self, id):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]
            
            return collection.find_one({"_id": id}).get("animal")

    def getAllUsers(self):
        with self.mongodb_context_man as conn:
            db = conn[self.db_name]
            collection = db["user"]

            return list(collection.find({}))