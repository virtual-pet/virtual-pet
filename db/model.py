import mongoengine as me

me.connect("virtual_pet")

class Animal(me.Document):
    image = me.BinaryField(required = True)
    animal_type = me.StringField(min_length=1, max_length=512, required = True)
    room_type = me.StringField(min_length=1, max_length=512, required = True)
    breed = me.StringField(min_length=1, max_length=512, required = False)
    sounds = me.ListField(max_length=100)
    greetings = me.ListField(min_length=1, max_length=512, required = True)


class User(me.Document):
    _id = me.IntField(primary_key=True, required=True)
    user_name = me.StringField(min_length=1, max_length=512, required = True)
    animal_name = me.StringField(min_length=1, max_length=512)
    animal = me.ReferenceField(Animal)
    a_food_level = me.IntField(min_value=0, max_value=100, default = 100)
    a_water_level = me.IntField(min_value=0, max_value=100, default = 100)

