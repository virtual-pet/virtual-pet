import os
from model import *


dir_path = os.path.dirname(os.path.realpath(__file__))
def seed_animal(anim_image, anim_type, room_type, breed, anim_sounds, anim_greetings):
    Animal.objects.create(image = anim_image, animal_type = anim_type, room_type = room_type, breed = breed, sounds = anim_sounds, greetings = anim_greetings)


def seed_all_animals(type):    
    with open(f"{dir_path}/texts/greetings.txt", encoding="utf8") as file_g:
        greetings = file_g.read().splitlines()
        
        with open(f"{dir_path}/texts/{type}/{type}_sounds.txt", encoding="utf8") as file_s:
            sounds = file_s.read().splitlines()
            
            for dir in os.listdir(f"{dir_path}/images/{type}"):
                for file in os.listdir(f"{dir_path}/images/{type}/{dir}"):
                    room_type = dir.split("_")[1]
                    breed = file.split(".")[0]

                    with open(f"{dir_path}/images/{type}/{dir}/{file}", "rb") as image_file:
                        image_data = image_file.read()
                        seed_animal(image_data, type, room_type, breed, sounds, greetings)

def seeder():
    seed_all_animals("cat")
    seed_all_animals("dog")
    seed_all_animals("hamster")

if __name__ == "__main__":
    seeder()